package com.example.statemachine.statemachinedemo;

import com.example.statemachine.statemachinedemo.config.CuestionarioEstados;
import com.example.statemachine.statemachinedemo.statemachine.Estados;
import com.example.statemachine.statemachinedemo.statemachine.Eventos;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.statemachine.StateContext;

import javax.swing.*;

@SpringBootApplication
@EnableAutoConfiguration
public class StatemachinedemoApplication {
	public static void main(String[] args) {
		new AnnotationConfigApplicationContext(CuestionarioEstados.class);
	}


}


