package com.example.statemachine.statemachinedemo.statemachine;

import com.example.statemachine.statemachinedemo.statemachine.Action.CandidatoPrestamo;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.model.*;
import org.springframework.statemachine.transition.TransitionKind;

import java.util.ArrayList;
import java.util.Collection;

public class CustomStateMachineModelFactory implements StateMachineModelFactory<Estados, Eventos> {

    @Override
    public StateMachineModel<Estados, Eventos> build() {
        ConfigurationData<Estados, Eventos> configurationData = new ConfigurationData<>();
        Collection<StateData<Estados, Eventos>> stateData = new ArrayList<>();
        stateData.add(new StateData<Estados, Eventos>(Estados.INICIO, true));
        stateData.add(new StateData<Estados, Eventos>(Estados.CANDIDATO));
        StatesData<Estados, Eventos> statesData = new StatesData<>(stateData);

        Collection<Action<Estados, Eventos>> actionsData = new ArrayList<>();
        actionsData.add(new CandidatoPrestamo());

        Collection<TransitionData<Estados, Eventos>> transitionData = new ArrayList<>();
        transitionData.add(new TransitionData<Estados, Eventos>(Estados.INICIO, Estados.CANDIDATO, Eventos.SI, actionsData,null, TransitionKind.INITIAL));
        TransitionsData<Estados, Eventos> transitionsData = new TransitionsData<>(transitionData);

        StateMachineModel<Estados, Eventos> stateMachineModel = new DefaultStateMachineModel<Estados, Eventos>(configurationData,
                statesData, transitionsData);
        return stateMachineModel;
    }

    @Override
    public StateMachineModel<Estados, Eventos> build(String machineId) {
        return build();
    }
}