package com.example.statemachine.statemachinedemo.statemachine.Action;

import com.example.statemachine.statemachinedemo.statemachine.Estados;
import com.example.statemachine.statemachinedemo.statemachine.Eventos;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import javax.swing.*;

public class CandidatoPrestamo implements Action<Estados, Eventos> {
    @Override
    public void execute(StateContext<Estados, Eventos> context) {
        int op = JOptionPane.showConfirmDialog(null, "Usted es candidato a un prestamo por 10,000, ¿desea aceptarlo?");
        context.getStateMachine().sendEvent(Eventos.values()[op]);
    }
}
