package com.example.statemachine.statemachinedemo.statemachine.Action;

import com.example.statemachine.statemachinedemo.statemachine.Estados;
import com.example.statemachine.statemachinedemo.statemachine.Eventos;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import javax.swing.*;

public class FinEncuesta implements Action<Estados, Eventos> {
    @Override
    public void execute(StateContext<Estados, Eventos> context) {

        if(Estados.INICIO == context.getSource().getId())
            JOptionPane.showMessageDialog(null, "Usted no es candidato");
        else if(Estados.CANDIDATO == context.getSource().getId()){
            if (context.getEvent() == Eventos.SI)
                JOptionPane.showMessageDialog(null, "Usted SI aceptó el prestamo");
            else
                JOptionPane.showMessageDialog(null, "Usted NO aceptó el prestamo");
        }
    }
}
