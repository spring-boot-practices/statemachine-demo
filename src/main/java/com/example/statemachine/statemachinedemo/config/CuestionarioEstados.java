package com.example.statemachine.statemachinedemo.config;

import com.example.statemachine.statemachinedemo.statemachine.CustomStateMachineModelFactory;
import com.example.statemachine.statemachinedemo.statemachine.Estados;
import com.example.statemachine.statemachinedemo.statemachine.Eventos;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineModelConfigurer;
import org.springframework.statemachine.config.model.StateMachineModelFactory;
import org.springframework.statemachine.listener.StateMachineListener;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;


@Configuration
//@ComponentScan("windoctor7")
@EnableStateMachine
public class CuestionarioEstados extends StateMachineConfigurerAdapter<Estados, Eventos> {
    @Override
    public void configure(StateMachineConfigurationConfigurer<Estados, Eventos> config) throws Exception {
        config
                .withConfiguration()
                .autoStartup(true) //la máquina de estados se iniciará automáticamente al correr la aplicación
                .listener(listener()); //Un listener (escuchador) que ocurrirá en cada cambio de estado.
    }
    @Override
    public void configure(StateMachineModelConfigurer<Estados, Eventos> model) throws Exception {
        model
            .withModel()
            .factory(modelFactory());

    }

    @Bean
    public StateMachineModelFactory<Estados, Eventos> modelFactory() {
        return new CustomStateMachineModelFactory();
    }

//
//    @Override
//    public void configure(StateMachineStateConfigurer<Estados, Eventos> states) throws Exception {
//        states
//                .withStates()
//                .initial(Estados.INICIO)
//                .state(Estados.INICIO,new PreguntaMayoriaEdad(),null)
//                .states(EnumSet.allOf(Estados.class))
//
//        ;
//    }
//
//    @Override
//    public void configure(StateMachineTransitionConfigurer<Estados, Eventos> transitions) throws Exception {
//        transitions
//                .withExternal()
//                //La transición de INICIO a CANDIDATO se lleva solo cuando ocurre el evento SI.
//                //De ocurrir la transición se ejecuta la acción new CandidatoPrestamo()
//                .source(Estados.INICIO).target(Estados.CANDIDATO).event(Eventos.SI).action(new CandidatoPrestamo())
//                .and()
//                .withExternal()
//                // La transición de INICIO a FIN se lleva cuando ocurre el evento NO.
//                // De ocurrir la transición se ejecuta la acción new FinEncuesta()
//                .source(Estados.INICIO).target(Estados.FIN).event(Eventos.NO).action(new FinEncuesta())
//                .and()
//                .withExternal()
//                //La transición de CANDIDATO a FIN se lleva cuando ocurre el evento SI
//                .source(Estados.CANDIDATO).target(Estados.FIN).event(Eventos.SI).action(new FinEncuesta())
//                .and()
//                .withExternal()
//                //La transición de CANDIDATO a FIN se lleva cuando ocurre el evento NO
//                .source(Estados.CANDIDATO).target(Estados.FIN).event(Eventos.NO).action(new FinEncuesta())
//        ;
//    }


    @Bean
    public StateMachineListener<Estados, Eventos> listener() {
        return new StateMachineListenerAdapter<Estados, Eventos>() {
            @Override
            public void stateChanged(State<Estados, Eventos> from, State<Estados, Eventos> to) {
                System.out.println(to.getId());
            }
        };
    }
}